package mono;
public class MonoPackageManager_Resources {
	public static String[] Assemblies = new String[]{
		/* We need to ensure that "calculaor.dll" comes first in this list. */
		"calculaor.dll",
		"Appium.Net.dll",
		"Castle.Core.dll",
		"Newtonsoft.Json.dll",
		"nunit.framework.dll",
		"SeleniumExtras.PageObjects.dll",
		"WebDriver.dll",
		"WebDriver.Support.dll",
		"Xamarin.Android.Support.Compat.dll",
		"Xamarin.Essentials.dll",
	};
	public static String[] Dependencies = new String[]{
	};
}
