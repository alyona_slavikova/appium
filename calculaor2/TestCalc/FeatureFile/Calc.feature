﻿Feature: Calculator
	As a person
	I want to use calculator
	In order to add two numbers and get qick answers

	As a pupil
	I want to use calculator on my phone
	So that I could substract numbers and get my correct change in supermarket

	As a student 
	I want to multiply numbers
	In order to get good mark on the test

	As an accountant 
	I want to divide numbers
	So I could get right numbers for tax report

	As a non-attentive person
	I want to delete misprinted numbers
	In order to type the correct ones

Scenario Outline: Add  two  numbers
	Given I press first number is <firstNum>
	And I press plus button
	And I press second number is <secondNum>
	When I press equal
	Then the result must be  <result>

	Examples:
		| firstNum | secondNum | result |
		| 5        | 5         | 10     |
		| 7        | 6         | 13     |

Scenario Outline: Substract  two  numbers
	Given I press first number is <firstNum>
	And I press substract button
	And I press second number is <secondNum>
	When I press equal
	Then the result must be  <result>

	Examples:
		| firstNum | secondNum | result |
		| 9        | 5         | 4      |
		| 8        | 9         | -1     |
		| 5        | 5         |  0     |

Scenario Outline: Multiply  two  numbers
	Given I press first number is <firstNum>
	And I press multiply button
	And I press second number is <secondNum>
	When I press equal
	Then the result must be  <result>

	Examples:
		| firstNum | secondNum | result |
		| 7        | 7         | 49     |
		| 2        | 0         |  0     |
		| 1        | 3         |  3     |

Scenario Outline: Divide  two  numbers
	Given I press first number is <firstNum>
	And I press divide button
	And I press second number is <secondNum>
	When I press equal
	Then the result must be  <result>

	Examples:
		| firstNum | secondNum | result |
		| 4        | 1         |  4     |
		| 5        | 2         |  2.5   |
		| 6        | 6         |  1     |

Scenario: Deleting misprinted number 
	Given I press button five
	Given I press plus button
	Given I press button nine
	And I press delete button
	And I press eight button
	And I press plus button
	And I press button five
	When I press equal
	Then the result is thirteen