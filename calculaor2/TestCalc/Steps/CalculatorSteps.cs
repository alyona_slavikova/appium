﻿using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;
using TestCalc.POM;

namespace TestCalc.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        AndroidDriver<AndroidElement> driver;
        public MainPage mainPage;
        //public Helper helper;
        [BeforeScenario]

        public void Driver()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_sEM61O");
            caps.AddAdditionalCapability("browserstack.key", "C7B7xopRoW46UBejqdtz");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://ee23bdc723e8e0a9e24ebeb173cc29ac467816e4");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above
            driver = new AndroidDriver<AndroidElement>(
                     new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
            mainPage = new MainPage(driver);
        }


        [Given(@"I press first number is (.*)")]
        public void GivenIPressFirstNumberIs(int p0)
        {
            mainPage.GetNumber(p0);

        }

        [Given(@"I press plus button")]
        public void GivenIPressPlusButton()
        {
            mainPage.ClickPlusBtn();
        }
        
        [Given(@"I press second number is (.*)")]
        public void GivenIPressSecondNumberIs(int p0)
        {
            mainPage.GetNumber(p0);
        }
        
        [When(@"I press equal")]
        public void WhenIPressEqual()
        {
            mainPage.ClickEqualBtn();

        }

        [Then(@"the result must be  (.*)")]
        public void ThenTheResultMustBe(double p0)
        {
            string actual = mainPage.Gettext();
            double result = Convert.ToDouble(actual);
            Assert.AreEqual(p0, result);

            
        }
        [Given(@"I press substract button")]
        public void GivenIPressSubstractButton()
        {
            mainPage.ClickMinusBtn();
        }

        [Given(@"I press multiply button")]
        public void GivenIPressMultiplyButton()
        {
            mainPage.ClickMultiplyBtn();
        }

        [Given(@"I press divide button")]
        public void GivenIPressDivideButton()
        {
            mainPage.ClickDivideBtn();
        }

        [Given(@"I press button five")]
        public void GivenIPressButtonFive()
        {
            mainPage.ClickFiveBtn();
        }

        [Given(@"I press button nine")]
        public void GivenIPressButtonNine()
        {
            mainPage.ClickNineBtn();
        }

        [Given(@"I press delete button")]
        public void GivenIPressDeleteButton()
        {
            mainPage.ClickDeleteBtn();
        }

        [Given(@"I press eight button")]
        public void GivenIPressEightButton()
        {
            mainPage.ClickEightBtn();
        }

        [Then(@"the result is thirteen")]
        public void ThenTheResultIsThirteen()
        {
            string actual = mainPage.Gettext();
            Assert.AreEqual("13", actual);
        }
        [AfterScenario]

        public void CloseDriver()
        {
            driver.Quit();
        }
    }
}
