﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestCalc.POM;

namespace TestCalc
{
    public static class Helper
    {
        public static MainPage mainPage;
        public static void GetNumber(int p0)
        {
            
            switch (p0)
            {
                case 1:
                    mainPage.ClickOneBtn();
                    break;
                case 2:
                    mainPage.ClickTwoBtn();
                    break;
                case 3:
                    mainPage.ClickThreeBtn();
                    break;
                case 4:
                    mainPage.ClickFourBtn();
                    break;
                case 5:
                    mainPage.ClickFiveBtn();
                    break;
                case 6:
                    mainPage.ClickSixBtn();
                    break;
                case 7:
                    mainPage.ClickSevenBtn();
                    break;
                case 8:
                    mainPage.ClickEightBtn();
                    break;
                case 9:
                    mainPage.ClickNineBtn();
                    break;
                case 0:
                    mainPage.ClickZeroBtn();
                    break;

            }
  
        }
    }
}
