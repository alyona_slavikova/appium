﻿Feature: Calculator
	As a student
	I want to use calculator
	In order to get qick answers

Scenario Outline: Add  two  numbers
	Given I press first number is <firstNum>
	And I press plus button
	And I press second number is <secondNum>
	When I press equal
	Then the result should be <result>

	Examples:
		| firstNum | secondNum | result |
		| 5        | 4         | 9      |
		| 67       | 3         | 70     |

