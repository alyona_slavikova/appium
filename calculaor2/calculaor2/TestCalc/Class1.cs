﻿using System;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using OpenQA.Selenium;
using NUnit.Framework;
using TestCalc.POM;

namespace TestCalc
{
    [TestFixture]
    public class Class1
    {
        
        AndroidDriver<AndroidElement> driver;
        public MainPage mainPage;
        [OneTimeSetUp]

        public void Driver()
        {
            AppiumOptions caps = new AppiumOptions();
            
            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "bsuser_sEM61O");
            caps.AddAdditionalCapability("browserstack.key", "C7B7xopRoW46UBejqdtz");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://ee23bdc723e8e0a9e24ebeb173cc29ac467816e4");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 3");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");


            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above
            driver = new AndroidDriver<AndroidElement>(
                     new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
            mainPage = new MainPage(driver);
        }


        //[Test]
        //public void Test()
        //{
        //    AndroidElement twoButton = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]"));
        //    twoButton.Click();
        //    AndroidElement plusButton = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]"));
        //    plusButton.Click();
        //    twoButton.Click();
        //    AndroidElement equalButton = driver.FindElement(By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]"));
        //    equalButton.Click();
        //    AndroidElement textField = driver.FindElement(By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView"));
        //    string actual = textField.Text;
        //    //Console.WriteLine(actual);
        //    Assert.AreEqual("4", actual);

        //    // Write your custom code here

        //    // Invoke driver.quit() after the test is done to indicate that the test is completed.

        //}
        //[Test]
        //public void FivePlusFour()
        //{
        //    //mainPage = new MainPage(driver);
        //    mainPage.ClickFiveBtn().ClickPlusBtn().ClickFourBtn().ClickEqualBtn();

        //    AndroidElement textField = driver.FindElement(By.XPath(" /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView"));
        //    string actual = textField.Text;
        //    Assert.AreEqual("9", actual);
        //}


        [OneTimeTearDown]

        public void QuitDriver()
        {
            driver.Quit();
        }
            
    }
}
