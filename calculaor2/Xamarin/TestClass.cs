﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Xamarin
{
    public class TestClass
    {
        [SetUp]
        public void BeforeEachTest()
        {
            AppInitializer.StartApp();
        }

        MainScreen _mainScreen = new MainScreen();
        [Test]
        public void Test()
        {

            _mainScreen.TapOnOne().TapOnPlus().TapOnTwo().TapOnEqual();
            Assert.AreEqual("3", _mainScreen.GettextFromField());
        }

    }
}
