﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace Xamarin.Steps
{
    [Binding]
    public class CalculatorSteps
    {
        MainScreen mainScreen = new MainScreen();
        [Given(@"I press first number is (.*)")]
        public void GivenIPressFirstNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }
        
        [Given(@"I press plus button")]
        public void GivenIPressPlusButton()
        {
            mainScreen.TapOnPlus();
        }
        
        [Given(@"I press second number is (.*)")]
        public void GivenIPressSecondNumberIs(int p0)
        {
            mainScreen.GetNumber(p0);
        }
        
        [Given(@"I press substract button")]
        public void GivenIPressSubstractButton()
        {
            mainScreen.TapOnMinus();
        }
        
        [Given(@"I press multiply button")]
        public void GivenIPressMultiplyButton()
        {
            mainScreen.TapOnMultiply();
        }
        
        [Given(@"I press divide button")]
        public void GivenIPressDivideButton()
        {
            mainScreen.TapOnDivide();
        }
        
        [Given(@"I press button five")]
        public void GivenIPressButtonFive()
        {
            mainScreen.TapOnFive();
        }
        
        [Given(@"I press button nine")]
        public void GivenIPressButtonNine()
        {
            mainScreen.TapOnNine();
        }
        
        [Given(@"I press delete button")]
        public void GivenIPressDeleteButton()
        {
            mainScreen.TapOnDel();
        }
        
        [Given(@"I press eight button")]
        public void GivenIPressEightButton()
        {
            mainScreen.TapOnEight();
        }
        
        [When(@"I press equal")]
        public void WhenIPressEqual()
        {
            mainScreen.TapOnEqual();
        }
        
        [Then(@"the result must be  (.*)")]
        public void ThenTheResultMustBe(double p0)
        {
            string actual = mainScreen.GettextFromField();
            double result = Convert.ToDouble(actual);
            Assert.AreEqual(p0, result);
        }
        
        [Then(@"the result is thirteen")]
        public void ThenTheResultIsThirteen()
        {
            string actual = mainScreen.GettextFromField();
            Assert.AreEqual("13", actual);
        }
    }
}
