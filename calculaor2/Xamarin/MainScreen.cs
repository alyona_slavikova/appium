﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.UITest;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace Xamarin
{
    public class MainScreen
    {
        public static IApp App => AppInitializer.App;
        public static void Repl()
        {
            App.Repl();
        }

        public Query textField = x => x.Id("input");
        public Query btnDEL = x => x.Marked("DEL");
        public Query btnZero = x => x.Marked("0");
        public Query btnOne = x => x.Marked("1");
        public Query btnTwo = x => x.Marked("2");
        public Query btnThree = x => x.Marked("3");
        public Query btnFour = x => x.Marked("4");
        public Query btnFive = x => x.Marked("5");
        public Query btnSix = x => x.Marked("6");
        public Query btnSeven = x => x.Marked("7");
        public Query btnEight = x => x.Marked("8");
        public Query btnNine = x => x.Marked("9");
        public Query btnDot = x => x.Marked(".");
        public Query btnPlus = x => x.Marked("+");
        public Query btnMinus = x => x.Marked("-");
        public Query btnMultiply = x => x.Marked("*");
        public Query btnDivide = x => x.Marked("/");
        public Query btnEqual = x => x.Marked("=");

        public MainScreen TapOnOne()
        {
            App.Tap(btnOne);
            return this;
        }

        public MainScreen TapOnDel()
        {
            App.Tap(btnDEL);
            return this;
        }

        public MainScreen TapOnZero()
        {
            App.Tap(btnZero);
            return this;
        }
        public MainScreen TapOnTwo()
        {
            App.Tap(btnTwo);
            return this;
        }
        public MainScreen TapOnThree()
        {
            App.Tap(btnThree);
            return this;
        }
        public MainScreen TapOnFour()
        {
            App.Tap(btnFour);
            return this;
        }
        public MainScreen TapOnFive()
        {
            App.Tap(btnFive);
            return this;
        }
        public MainScreen TapOnSix()
        {
            App.Tap(btnSix);
            return this;
        }
        public MainScreen TapOnSeven()
        {
            App.Tap(btnSeven);
            return this;
        }
        public MainScreen TapOnEight()
        {
            App.Tap(btnEight);
            return this;
        }
        public MainScreen TapOnNine()
        {
            App.Tap(btnNine);
            return this;
        }
        public MainScreen TapOnDot()
        {
            App.Tap(btnDot);
            return this;
        }
        public MainScreen TapOnPlus()
        {
            App.Tap(btnPlus);
            return this;
        }
        public MainScreen TapOnMinus()
        {
            App.Tap(btnMinus);
            return this;
        }
        public MainScreen TapOnMultiply()
        {
            App.Tap(btnMultiply);
            return this;
        }
        public MainScreen TapOnDivide()
        {
            App.Tap(btnDivide);
            return this;
        }
        public MainScreen TapOnEqual()
        {
            App.Tap(btnEqual);
            return this;
        }
        public string GettextFromField()
        {
            return App.Query(textField)[0].Text.Trim();
        }

        public void GetNumber(int p0)
        {

            switch (p0)
            {
                case 1:
                    TapOnOne();
                    break;
                case 2:
                    TapOnTwo();
                    break;
                case 3:
                    TapOnThree();
                    break;
                case 4:
                    TapOnFour();
                    break;
                case 5:
                    TapOnFive();
                    break;
                case 6:
                    TapOnSix();
                    break;
                case 7:
                    TapOnSeven();
                    break;
                case 8:
                    TapOnEight();
                    break;
                case 9:
                    TapOnNine();
                    break;
                case 0:
                    TapOnZero();
                    break;
                default:
                    break;

            }

        }

    }
    

}
